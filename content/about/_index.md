---
title: "About"
date: 2022-01-08T10:41:03+06:00
subTitle: >
        Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Cras ultricies ligula sed magna dictum porta.
sliderImage:
  - image: "images/stor/story-01.jpg"
  - image: "images/stor/story-01.jpg"
  - image: "images/stor/story-01.jpg"
---
## This Is Our Story.

We’re here for those who refuse to settle for trustfalls, and slideshows.  Who continue to search for new
ideas, and better experiences in their team building exercises. Because today’s employees don't want to just slay their OKRs, they want to slay some dragons.
They want to see Brenda from accounting save the group from some Eldritch Horror.  Let them unleash their creativity, and bond together over an epic journey.
